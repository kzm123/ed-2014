package pl.edu.agh.mn.ed.lp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import org.jgraph.graph.DefaultEdge;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.SimpleGraph;

public class Main {

	public static class IntervalEndMarker{
		private int year;
		private int month;
		private int day;
		
		public Timestamp getTimestamp(){
			return new Timestamp(year - 1900, month, day, 0, 0, 0, 0);
		}

		public IntervalEndMarker(int year, int month, int day) {
			this.year = year;
			this.month = month;
			this.day = day;
		}
	}
	
	public static UndirectedGraph<Integer,DefaultEdge> extractFromInterval(IntervalEndMarker endMarker){
		SimpleGraph<Integer,DefaultEdge> resultGraph = null;
		try {
			Set<Integer> addedUsers = new HashSet<Integer>();
			
			resultGraph = new SimpleGraph<Integer, DefaultEdge>(DefaultEdge.class);
			
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/social", "social", "social");
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select user1_id, user2_id from social_friendshiplink where timestamp <= '" + endMarker.getTimestamp() + "'::timestamp or timestamp is null");
			while(resultSet.next()){
				int user1 = resultSet.getInt(1);
				int user2 = resultSet.getInt(2);
				
				if(!addedUsers.contains(user1)){
					addedUsers.add(user1);
					resultGraph.addVertex(user1);
				}
				if(!addedUsers.contains(user2)){
					addedUsers.add(user2);
					resultGraph.addVertex(user2);
				}
				resultGraph.addEdge(user1, user2);
			}
		
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return resultGraph;
	}
	
	public static void main(String[] args) {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		}
		System.out.println(extractFromInterval(new IntervalEndMarker(2006,12,0)));
	}

}
