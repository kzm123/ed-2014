CREATE SEQUENCE edge_to_predict_idx
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE predicted_edge_idx
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


CREATE TABLE edges_to_predict (
    idx bigint DEFAULT nextval('edge_to_predict_idx') NOT NULL,
    v1 integer NOT NULL,
    v2 integer NOT NULL
);


CREATE TABLE predicted_edges (
    idx integer DEFAULT nextval('predicted_edge_idx') NOT NULL,
    v1 integer NOT NULL,
    v2 integer NOT NULL,
    rank double precision NOT NULL
);
