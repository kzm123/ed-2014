create temporary table t (user1 integer, user2 integer, direction integer, timestamp integer);

\copy t from '/tmp/out2.epinions' delimiter ' ' csv;

insert into friendship_links (user1, user2, creation_timestamp, network_id)
  select u1, u2, to_timestamp(min(timestamp)), 3
  from (select least(user1, user2) as u1, greatest(user1, user2) as u2, timestamp,
               (least(user1, user2)::text || greatest(user1, user2)::text) as link_id from t) t1
  group by u1, u2, link_id;

delete from friendship_links where user1 = user2;
