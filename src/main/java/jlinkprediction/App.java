package jlinkprediction;


import jlinkprediction.similarity.VertexSimilarityIndex;

public class App {

    private static final String CONNECTION_STRING = "jdbc:postgresql://localhost:5432/social";
    private static final String USERNAME = "social";
    private static final String PASSWORD = "social";
    private static final String NETWORK_NAME = "mtest";

    public static void main(String[] args) {
       /* GraphLoader loader = new PostgresDbGraphLoader(CONNECTION_STRING, USERNAME, PASSWORD);

        ModifiableSocialGraph fullGraph = loader.load(NETWORK_NAME);
        GraphPartitionResult partitionedGraph = fullGraph.divideGraph(GraphPartitionStrategyFabric.EdgesNumberBasedDivision(80, .1f));

        SocialGraph initialGraph = partitionedGraph.initialPart;
        SocialGraph finalGraph = partitionedGraph.finalPart;

        Rater rater = new PrecisionRater();
        Predictor predictor = new Predictor();

        System.out.println("\n-------------------------\nStart predict process using local similarity indexes\n");

        ArrayList<SimilarityIndexRank> indiciesRanks = new ArrayList<SimilarityIndexRank>();
        for(VertexSimilarityIndex si :LocalSimilarityIndex.values()) {

            System.out.printf("using %-35s : ", "'" + si.indexName() + "'");

            SimilarityIndexRank ir = new SimilarityIndexRank();
            ir.index = si;
            PredictionResultHolder resultsHolder = predictor.withSimilarityIdx(si).withResultFilter(new RankRelativeThresholdFilter(.7)).predictLinks(initialGraph);
            ir.rate = rater.rate(finalGraph, resultsHolder);
            indiciesRanks.add(ir);

            System.out.println("FINISHED");
        }

        Collections.sort(indiciesRanks, new Comparator<SimilarityIndexRank>() {
            public int compare(SimilarityIndexRank arg0, SimilarityIndexRank arg1) {
                if (arg0.rate == arg1.rate)
                    return 0;
                else if (arg0.rate > arg1.rate)
                    return -1;
                else
                    return 1;
            }
        });

        System.out.println("\n\nResults:\n");
        for (SimilarityIndexRank i : indiciesRanks) {
            System.out.printf("%-35s %3.5f%%\n", i.index.indexName(), i.rate);
        }
*/
    }

    static class SimilarityIndexRank {
        double rate;
        VertexSimilarityIndex index;
    }

}
