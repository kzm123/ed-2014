package jlinkprediction;


import jlinkprediction.db.DbConfig;

public class AppConfig {
    public static final String CONNECTION_STRING = "jdbc:postgresql://localhost:5432/social";
    public static final String USERNAME = "social";
    public static final String PASSWORD = "social";
    public static final String NETWORK_NAME = "usair";
//    public static final String NETWORK_NAME = "Epinions trust";

    public static final int PREDICTED_EDGES_BUFF_FLUSH_THRESHOLD = 2000000;
    public static final int PREDICTED_EDGES_ITERATOR_CHUNK_SIZE = 5000000;
    public static final int PREDICTOR_EDGES_CHUNK_SIZE = 10000;

    public static final DbConfig DB_CONFIG = new DbConfig(CONNECTION_STRING, USERNAME, PASSWORD);
}
