package jlinkprediction;


import jlinkprediction.db.DbConfig;
import jlinkprediction.graph.SocialGraph;
import jlinkprediction.graph.load.db.PostgresDbGraphLoader;
import jlinkprediction.graph.missing.MissingEdgesProviders;
import jlinkprediction.graph.partition.GraphPartitionResult;
import jlinkprediction.graph.partition.ModifiableSocialGraph;
import jlinkprediction.graph.partition.strategy.GraphPartitionStrategies;
import jlinkprediction.predict.PredictionConfig;
import jlinkprediction.predict.Predictor;
import jlinkprediction.predict.filter.ResultFilters;
import jlinkprediction.predict.rate.PrecisionRater;
import jlinkprediction.predict.rate.Rater;
import jlinkprediction.predict.store.ResultHolder;
import jlinkprediction.similarity.LocalSimilarityIndex;

import java.util.HashMap;
import java.util.Map;

public class Report {

    private final GraphPartitionStrategies.GraphPartitionStrategy partitionStrategy;
    private final ModifiableSocialGraph fullGraph;
    private final Predictor predictor;
    private final Rater rater;
    private final MissingEdgesProviders.MissingEdgesProvider edgesProvider;


    public Report( ModifiableSocialGraph fullGraph, GraphPartitionStrategies.GraphPartitionStrategy partitionStrategy, Predictor predictor, Rater rater, MissingEdgesProviders.MissingEdgesProvider edgesProvider) {
        this.partitionStrategy = partitionStrategy;
        this.fullGraph = fullGraph;
        this.predictor = predictor;
        this.rater = rater;
        this.edgesProvider = edgesProvider;
    }

    public void printReport(int iterationsCount, LocalSimilarityIndex indices[], ResultFilters.ResultFilter[] resultFilters){

        Map<LocalSimilarityIndex, Map<ResultFilters.ResultFilter, Double>> productivity = new HashMap<LocalSimilarityIndex, Map<ResultFilters.ResultFilter, Double>>();
        for(LocalSimilarityIndex si : indices){
            HashMap<ResultFilters.ResultFilter, Double> m = new HashMap<ResultFilters.ResultFilter, Double>();
            for(ResultFilters.ResultFilter rf : resultFilters){
                m.put(rf, 0d);
            }
            productivity.put(si, m);
        }



        for(int iteration = 1; iteration <= iterationsCount; ++iteration) {
            System.out.println("ITERATION #" + iteration);

            System.out.print("Partitioning of graph:");
            GraphPartitionResult partitionedGraph = fullGraph.divideGraph(partitionStrategy);
            SocialGraph initialGraph = partitionedGraph.initialPart;
            SocialGraph finalGraph = partitionedGraph.finalPart;
            PredictionConfig pConfig = PredictionConfig.NewConfig(initialGraph, edgesProvider);
            System.out.println("FINISHED");

            for (LocalSimilarityIndex si : indices) {
                Map<ResultFilters.ResultFilter, Double> indexMap = productivity.get(si);
                System.out.printf("#using %-35s : ", "'" + si.indexName() + "'");

                ResultHolder results = predictor.withSimilarityIdx(si).predictLinks(pConfig);

                System.out.print(" PREDICTED ");
                for (ResultFilters.ResultFilter rf : resultFilters) {
                    indexMap.put(rf, indexMap.get(rf) + rater.rate(finalGraph, results, rf));
                }

                System.out.println("RATED");
            }
        }

        System.out.printf("%-40s", " ");
        for(ResultFilters.ResultFilter rf : resultFilters){
            System.out.printf("%-40s", rf.label());
        }
        System.out.println();
        for(LocalSimilarityIndex si : indices){
            System.out.printf("%-40s", "'" + si.indexName() + "'");
            for(ResultFilters.ResultFilter rf : resultFilters){
                System.out.printf("%-40s", new StringBuilder().append(productivity.get(si).get(rf) / iterationsCount));
            }
            System.out.println();
        }

    }
    public static void main(String[] args) {
        final DbConfig conf = new DbConfig(
                AppConfig.CONNECTION_STRING,
                AppConfig.USERNAME,
                AppConfig.PASSWORD
        );

        ModifiableSocialGraph fullGraph = new PostgresDbGraphLoader(conf).load(AppConfig.NETWORK_NAME);
//        ModifiableSocialGraph fullGraph = new PostgresDbGraphLoader(conf).loadFromInterval(AppConfig.NETWORK_NAME, new TimeMarker(2002,11,1), new TimeMarker(2003, 1, 1));
        Report report = new Report(
                fullGraph,
//                GraphPartitionStrategies.EdgesCreationTimeBasedDivision(new TimeMarker(2002, 12, 2)),
                GraphPartitionStrategies.EdgesNumberBasedDivision(90, 0.9f),
                new Predictor(),
                new PrecisionRater(),
                MissingEdgesProviders.Complementary()
        );

        report.printReport(
                10,
               // new LocalSimilarityIndex[] {LocalSimilarityIndex.CN},
                LocalSimilarityIndex.values(),
                new ResultFilters.ResultFilter[]{
                        ResultFilters.BestRankRelative(.9f),
                        ResultFilters.BestRanked(25)
                }
        );
    }
}
