package jlinkprediction.db;

public class DbConfig {
    public final String connectionString;
    public final String username;
    public final String password;

    public DbConfig(String connectionString, String username, String password) {
        this.connectionString = connectionString;
        this.username = username;
        this.password = password;
    }

}