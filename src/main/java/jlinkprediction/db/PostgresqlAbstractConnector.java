package jlinkprediction.db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class PostgresqlAbstractConnector {

    protected final DbConfig dbConf;

    public static interface StatementConsumer {
        public void consumeStatement(Statement statement) throws SQLException;
    }

    public PostgresqlAbstractConnector(DbConfig config) {
        this.dbConf = config;
    }

    protected void statementInvocation(StatementConsumer consumer){
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbConf.connectionString, dbConf.username, dbConf.password);
            Statement statement = connection.createStatement();

            consumer.consumeStatement(statement);
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
