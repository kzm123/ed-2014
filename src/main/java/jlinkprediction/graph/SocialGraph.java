package jlinkprediction.graph;

import toools.set.IntSet;

import java.util.Iterator;

public interface SocialGraph {
    public int commonNeighboursCount(int vertex1, int vertex2);

    public IntSet commonNeighbours(int vertex1, int vertex2);

    public int allNeighboursCount(int vertex1, int vertex2);

    public IntSet allNeighbours(int vertex1, int vertex2);

    public int getVertexDegree(int vertex);

    public IntSet getVertices();

    public boolean isConnected();

    public boolean hasEdgeBetween(int vertex1, int vertex2);

    public String getNetworkName();

    public Iterator<VertexPair> getEdges();

    public int getEdgesCount();

    public String getDescription();
}
