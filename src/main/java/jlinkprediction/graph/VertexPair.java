package jlinkprediction.graph;

public class VertexPair implements Comparable<VertexPair>{
    public final int firstVertex;
    public final int secondVertex;

    public VertexPair(int firstVertex, int secondVertex) {
        if(firstVertex < secondVertex) {
            this.firstVertex = firstVertex;
            this.secondVertex = secondVertex;
        }else{
            this.firstVertex = secondVertex;
            this.secondVertex = firstVertex;
        }
    }

    @Override
    public int hashCode() {
        return firstVertex + secondVertex;
    }

    @Override
    public boolean equals(Object obj) {
        VertexPair o = (VertexPair) obj;
        return o.firstVertex == firstVertex && o.secondVertex == secondVertex || o.firstVertex == secondVertex && o.secondVertex == firstVertex;
    }

    @Override
    public String toString() {
        return firstVertex + "-" + secondVertex;
    }


    @Override
    public int compareTo(VertexPair o) {
        if(firstVertex > o.firstVertex)
            return 1;
        else if( firstVertex < o.firstVertex){
            return -1;
        }else if(secondVertex > o.secondVertex){
            return 1;
        }else if(secondVertex < o.secondVertex){
            return -1;
        }else{
            return 0;
        }
    }
}
