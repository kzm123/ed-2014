package jlinkprediction.graph.impl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import grph.Grph;
import jlinkprediction.graph.VertexPair;
import jlinkprediction.graph.partition.ModifiableSocialGraph;
import toools.set.IntHashSet;
import toools.set.IntSet;

import java.util.Iterator;
import java.util.concurrent.ExecutionException;

class InMemoryGRPHSocialGraph extends ModifiableSocialGraph {
    private static final long serialVersionUID = -2900490538404613848L;
    private final String networkName;
    private final String description;
    private int edgesIdCounter = 0;
    private InMemoryGrphCorrect wrappedGraph = new InMemoryGrphCorrect();

    private LoadingCache<Integer, IntSet> vertexNeighbourSet = CacheBuilder.newBuilder()
            .maximumSize(50000)
            .build(
                    new CacheLoader<Integer, IntSet>() {
                        @Override
                        public IntSet load(Integer integer) throws Exception {
                            return wrappedGraph.getNeighbours(integer);
                        }
                    }
            );


    public InMemoryGRPHSocialGraph(String networkName, String description) {
        this.networkName = networkName;
        this.description = description;
    }

    @Override
    public boolean hasEdgeBetween(int vertex1, int vertex2) {
        return wrappedGraph.getEdgesConnecting(vertex1, vertex2).size() > 0;
    }

    @Override
    public IntSet getVertices() {
        return wrappedGraph.getVertices();
    }

    @Override
    public boolean isConnected() {
        return wrappedGraph.isConnected();
    }

    @Override
    public Iterator<VertexPair> getEdges() {
        return new EdgesIterator(wrappedGraph);
    }

    @Override
    public int getEdgesCount() {
        return wrappedGraph.getEdges().size();
    }

    @Override
    public int addEdgeBetween(int vertex1, int vertex2) {
        int edgeId = edgesIdCounter++;
        wrappedGraph.addUndirectedSimpleEdge(edgeId, vertex2, vertex1);
        return edgeId;
    }

    @Override
    protected int getEdgeId(int vertex1, int vertex2) {
        IntSet edges = wrappedGraph.getEdgesConnecting(vertex1, vertex2);
        if(edges.size() == 0) return -1;
        return edges.toIntArray()[0];
    }

    @Override
    public IntSet commonNeighbours(int vertex1, int vertex2) {
        IntSet common = null;

        try {
            IntSet v1Neighbours = vertexNeighbourSet.get(vertex1);
            IntSet v2Neighbours = vertexNeighbourSet.get(vertex2);

            common = new IntHashSet();
            common.addAll(v1Neighbours);
            common.retainAll(v2Neighbours);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return common;
    }

    @Override
    public IntSet allNeighbours(int vertex1, int vertex2) {
        IntSet all = new IntHashSet();

        try {
            IntSet v1Neighbours = vertexNeighbourSet.get(vertex1);
            IntSet v2Neighbours = vertexNeighbourSet.get(vertex2);


            all.addAll(v1Neighbours);
            all.addAll(v2Neighbours);
        }catch (ExecutionException e){
            e.printStackTrace();
        }

        return all;
    }

    @Override
    public int getVertexDegree(int vertex) {
        return wrappedGraph.getVertexDegree(vertex);
    }

    @Override
    public int commonNeighboursCount(int vertex1, int vertex2) {
        return commonNeighbours(vertex1, vertex2).size();
    }

    @Override
    public int allNeighboursCount(int vertex1, int vertex2) {
        return allNeighbours(vertex1, vertex2).size();
    }

    @Override
    public String getNetworkName() {
        return networkName;
    }

    @Override
    public String getDescription() {
        return description;
    }

    private class EdgesIterator implements Iterator<VertexPair> {

        private Iterator<grph.VertexPair> innerIterator;

        public EdgesIterator(Grph graph) {
            innerIterator = graph.getEdgePairs().iterator();
        }

        public boolean hasNext() {
            return innerIterator.hasNext();
        }

        public VertexPair next() {
            final grph.VertexPair n = innerIterator.next();
            return new VertexPair(n.first, n.second);
        }

        public void remove() {
        }
    }

    @Override
    public void addVertex(int vertex) {
        wrappedGraph.addVertex(vertex);
    }
}
