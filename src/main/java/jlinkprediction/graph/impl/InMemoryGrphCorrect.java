package jlinkprediction.graph.impl;

import grph.in_memory.InMemoryGrph;

public class InMemoryGrphCorrect extends InMemoryGrph {
    @Override
    public boolean isMixed() {
        return false;
    }

    public InMemoryGrphCorrect(){
        super();
    }
}