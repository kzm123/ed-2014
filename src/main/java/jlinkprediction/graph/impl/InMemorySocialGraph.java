package jlinkprediction.graph.impl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import jlinkprediction.graph.VertexPair;
import jlinkprediction.graph.partition.ModifiableSocialGraph;
import toools.set.IntHashSet;
import toools.set.IntSet;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;

class InMemorySocialGraph extends ModifiableSocialGraph {

    private Map<Integer, IntSet> neighbours = new HashMap<Integer, IntSet>();
    private Map<VertexPair, Integer> edgesIds = new HashMap<VertexPair, Integer>();
    private int edgesIdCounter = 0;

    private final String networkName;
    private final String networkDescription;


    private final LoadingCache<Integer, IntSet> neighboursCache = CacheBuilder.newBuilder()
            .maximumSize(100000)
            .build(
                    new CacheLoader<Integer, IntSet>() {
                        @Override
                        public IntSet load(Integer vertexIdx) throws Exception {
                            return neighbours.get(vertexIdx);
                        }
                    }
            );

    private final LoadingCache<Integer, Integer> vertexDegreeCache = CacheBuilder.newBuilder()
            .maximumSize(1000000)
            .build(
                    new CacheLoader<Integer, Integer>() {
                        @Override
                        public Integer load(Integer vertex) throws Exception {
                            return neighbours.get(vertex).size();
                        }
                    }
            );



    public InMemorySocialGraph(String networkName, String networkDescription) {
        this.networkName = networkName;
        this.networkDescription = networkDescription;
    }

    @Override
    public void addVertex(int vertex) {
	    this.neighbours.put(vertex, new IntHashSet());
    }

    @Override
    public int addEdgeBetween(int vertex1, int vertex2) {
        int edgeId = edgesIdCounter++;
        edgesIds.put(new VertexPair(vertex1, vertex2), edgeId);

        IntSet v1neibs = neighbours.get(vertex1);
        IntSet v2neibs = neighbours.get(vertex2);

        if(v1neibs == null){
            v1neibs = new IntHashSet();
            neighbours.put(vertex1, v1neibs);
        }

        if(v2neibs == null){
            v2neibs = new IntHashSet();
            neighbours.put(vertex2, v2neibs);
        }

        v1neibs.add(vertex2);
        v2neibs.add(vertex1);

        return edgeId;
    }

    @Override
    protected int getEdgeId(int vertex1, int vertex2) {
	    return edgesIds.get(new VertexPair(vertex1, vertex2)).intValue();
    }

    @Override
    public int commonNeighboursCount(int vertex1, int vertex2) {
	    return commonNeighbours(vertex1, vertex2).size();
    }

    @Override
    public IntSet commonNeighbours(int vertex1, int vertex2) {
        IntSet common = null;

        try {
            IntSet v1Neighbours = neighboursCache.get(vertex1);
            IntSet v2Neighbours = neighboursCache.get(vertex2);

            common = new IntHashSet();
            common.addAll(v1Neighbours);
            common.retainAll(v2Neighbours);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return common;
    }

    @Override
    public int allNeighboursCount(int vertex1, int vertex2) {
	    return allNeighbours(vertex1, vertex2).size();
    }

    @Override
    public IntSet allNeighbours(int vertex1, int vertex2) {
        IntSet all = new IntHashSet();

        try {
            IntSet v1Neighbours = neighboursCache.get(vertex1);
            IntSet v2Neighbours = neighboursCache.get(vertex2);

            all.addAll(v1Neighbours);
            all.addAll(v2Neighbours);
        }catch (ExecutionException e){
            e.printStackTrace();
        }

        return all;
    }

    @Override
    public int getVertexDegree(int vertex) {
        try {
            return vertexDegreeCache.get(vertex);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean hasEdgeBetween(int vertex1, int vertex2) {
        VertexPair pair = new VertexPair(vertex1, vertex2);
        final IntSet neighbourhood = neighbours.get(pair.firstVertex);
        return neighbourhood != null && neighbourhood.contains(pair.secondVertex);
    }

    @Override
    public String getNetworkName() {
	    return networkName;
    }

    @Override
    public Iterator<VertexPair> getEdges() {
	    return edgesIds.keySet().iterator();
    }

    @Override
    public int getEdgesCount() {
	    return edgesIds.size();
    }

    @Override
    public IntSet getVertices() {
	    IntSet allVertices = new IntHashSet();

        for(Integer vertex : neighbours.keySet()){
            allVertices.add(vertex);
        }

        return allVertices;
    }

    @Override
    public String getDescription() {
        return networkDescription;
    }

    @Override
    public boolean isConnected() {
        throw new RuntimeException("Not implemented");
    }
}
