package jlinkprediction.graph.impl;

import jlinkprediction.graph.partition.ModifiableSocialGraph;

public class SocialGraphs {

    public static ModifiableSocialGraph Default(String networkName, String networkDescription){
        return InMemoSimple(networkName, networkDescription);
    }

    public static ModifiableSocialGraph InMemoGRPH(String networkName, String networkDescription){
        return new InMemoryGRPHSocialGraph(networkName, networkDescription);
    }

    public static ModifiableSocialGraph InMemoSimple(String networkName, String networkDescription){
        return new InMemorySocialGraph(networkName, networkDescription);
    }
}
