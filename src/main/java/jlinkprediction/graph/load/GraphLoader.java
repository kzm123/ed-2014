package jlinkprediction.graph.load;


import jlinkprediction.graph.partition.ModifiableSocialGraph;
import jlinkprediction.graph.partition.strategy.TimeMarker;

public interface GraphLoader {
     public ModifiableSocialGraph load(String socialNetworkName);
     public ModifiableSocialGraph loadFromInterval(String  socialNetworkName, TimeMarker intBegin, TimeMarker intEnd);
}
