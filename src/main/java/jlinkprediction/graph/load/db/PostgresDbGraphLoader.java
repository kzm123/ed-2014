package jlinkprediction.graph.load.db;

import jlinkprediction.db.DbConfig;
import jlinkprediction.db.PostgresqlAbstractConnector;
import jlinkprediction.graph.impl.SocialGraphs;
import jlinkprediction.graph.load.GraphLoader;
import jlinkprediction.graph.partition.ModifiableSocialGraph;
import jlinkprediction.graph.partition.strategy.TimeMarker;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Optional;

public class PostgresDbGraphLoader extends PostgresqlAbstractConnector implements GraphLoader {


    public PostgresDbGraphLoader(DbConfig config) {
        super(config);
    }

    private abstract class LoadStatementConsumer implements StatementConsumer {

        private final String networkName;
        private ModifiableSocialGraph result;

        public LoadStatementConsumer(String networkName) {
            this.networkName = networkName;
        }

        public ModifiableSocialGraph getResult() {
            return result;
        }

        protected abstract Optional<String> additionalConstraints();

        @Override
        public void consumeStatement(Statement statement) throws SQLException {
            ResultSet resultSet = statement.executeQuery("select name, description from v_social_network where lower(name) = lower('" + networkName + "');");
            if (!resultSet.next()) {
                throw new RuntimeException("Social network  '" + networkName + "' wasn't found in database : " + dbConf.connectionString);
            }

            result = SocialGraphs.Default(resultSet.getString(1), resultSet.getString(2));

            Optional<String> addConstraints = additionalConstraints();

            StringBuilder query = new StringBuilder()
                    .append("select user1, user2, creation_timestamp from friendship_links")
                    .append(" where network_id = (")
                    .append("select id from social_network ")
                    .append("where name = '").append(result.getNetworkName())
                    .append("') ")
                    .append(addConstraints.isPresent() ? " and " + addConstraints.get() : "")
                    ;

            System.out.println(query.toString());
            resultSet = statement.executeQuery(query.toString());

            System.out.print("Network is loading into memory : ");

            while (resultSet.next()) {
                Timestamp createdAt = resultSet.getTimestamp(3);
                if (createdAt == null) {
                    result.addEdgeBetween(resultSet.getInt(1), resultSet.getInt(2));
                } else {
                    result.addEdgeBetween(resultSet.getInt(1), resultSet.getInt(2), createdAt.getTime());
                }
            }

            System.out.println("\tFINISHED");

        }
    }

    @Override
    public ModifiableSocialGraph load(String networkName) {
        LoadStatementConsumer statementConsumer = new LoadStatementConsumer(networkName){
            @Override
            protected Optional<String> additionalConstraints() {
                return Optional.empty();
            }
        };
        statementInvocation(statementConsumer);
        return statementConsumer.getResult();
    }


    @Override
    public ModifiableSocialGraph loadFromInterval(String networkName, final TimeMarker intBegin, final TimeMarker intEnd) {
        LoadStatementConsumer statementConsumer = new LoadStatementConsumer(networkName){
            @Override
            protected Optional<String> additionalConstraints() {
                return Optional.of("creation_timestamp is null or (creation_timestamp >= '" + intBegin.getTimestamp() + "'::timestamp and creation_timestamp <= '" + intEnd.getTimestamp() + "'::timestamp)");
            }
        };
        statementInvocation(statementConsumer);
        return statementConsumer.getResult();
    }
}