package jlinkprediction.graph.missing;


import jlinkprediction.AppConfig;
import jlinkprediction.graph.SocialGraph;
import jlinkprediction.graph.VertexPair;

import java.util.ArrayList;
import java.util.List;

abstract class AbstractMissingEdgesProvider implements MissingEdgesProviders.MissingEdgesProvider {

    private static final int LOCAL_EDGES_TO_PREDICT_BUFF_FLUSH_THRESHOLD = 5000000;
    private Thread[] threads;

    protected abstract List<VertexPair> edgesForVertex(int currVertexIdx, int[] allVertices, SocialGraph graph);

    private class ConcurrentWorker extends Thread{

        private final int threadIdx;
        private final int[] allVertices;
        private final SocialGraph graph;
        private final MissingEdgesHolder edgesStorage;
        private ArrayList<VertexPair> localBuffer;

        @Override
        public void run() {
            for(int currVertexIdx = threadIdx; currVertexIdx < allVertices.length; currVertexIdx += threads.length){
                localBuffer.addAll(edgesForVertex(currVertexIdx, allVertices, graph));
                if(localBuffer.size() > LOCAL_EDGES_TO_PREDICT_BUFF_FLUSH_THRESHOLD) {
                    edgesStorage.storeElements(localBuffer);
                    localBuffer = new ArrayList<VertexPair>();
                }
            }
            edgesStorage.storeElements(localBuffer, true);
        }

        public ConcurrentWorker(MissingEdgesHolder edgesStorage, SocialGraph graph, int[] allVertices, int threadIdx) {
            setDaemon(true);
            
            this.allVertices = allVertices;
            this.threadIdx = threadIdx;
            this.graph = graph;    
            this.edgesStorage = edgesStorage;
            this.localBuffer = new ArrayList<VertexPair>();
        }
    }

    @Override
    public MissingEdgesHolder getMissingEdges(SocialGraph graph) {
        int[] vertices = graph.getVertices().toIntArray();
        threads = new Thread[Runtime.getRuntime().availableProcessors()];
        
        final long allEdges = ((long) (vertices.length / 2)) * ((long) (vertices.length - 1)) - graph.getEdgesCount();
        MissingEdgesHolder edgesStorage = new MissingEdgesHolder(AppConfig.DB_CONFIG, allEdges);
        System.out.println("Edges to predict : " + allEdges);

        for(int thrIdx = 0; thrIdx < threads.length; ++thrIdx){
            threads[thrIdx] = new ConcurrentWorker(edgesStorage, graph, vertices, thrIdx);
            threads[thrIdx].start();
        }

        return edgesStorage;
    }
}
