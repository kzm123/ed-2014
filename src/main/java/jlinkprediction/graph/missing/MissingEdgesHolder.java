package jlinkprediction.graph.missing;


import jlinkprediction.db.DbConfig;
import jlinkprediction.graph.VertexPair;
import jlinkprediction.tools.ChunkIterator;
import jlinkprediction.tools.storage.DbRecordBufferedStorage;
import jlinkprediction.tools.storage.StorageQueryConfig;
import jlinkprediction.tools.storage.StoredRecordsUnorderedIterator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MissingEdgesHolder extends DbRecordBufferedStorage<VertexPair> {
    private static final int BUFF_FLUSH_SIZE = 1000000;
    private final ReentrantLock lock;
    private final Condition notifier;

    public MissingEdgesHolder(DbConfig conf, long expectedElementsCount) {
        super(conf, new StorageQueryConfig<VertexPair>("edges_to_predict", "edge_to_predict_idx") {
            @Override
            protected String dbRecordValueToStore(VertexPair record) {
                return record.firstVertex + "," + record.secondVertex;
            }

            @Override
            protected String dbRecordValueAttributeNamesToStore() {
                return "v1,v2";
            }

            @Override
            protected VertexPair createFromQueryResult(ResultSet resultSet) throws SQLException {
                return new VertexPair(resultSet.getInt(1), resultSet.getInt(2));
            }

            @Override
            protected String loadOrderedQueryPart() {
                return "";
            }
        }, BUFF_FLUSH_SIZE, BUFF_FLUSH_SIZE, expectedElementsCount);

        this.lock = new ReentrantLock();
        this.notifier = this.lock.newCondition();
    }

    @Override
    public void storeElements(Collection<VertexPair> elementsToStore) {
        try{
            lock.lock();
            super.storeElements(elementsToStore);
            notifier.signalAll();
        }finally {
            lock.unlock();
        }
    }

    @Override
    public void storeElements(Collection<VertexPair> elementsToStore, boolean forceFlush) {
        try{
            lock.lock();
            super.storeElements(elementsToStore, forceFlush);
            notifier.signalAll();
        }finally {
            lock.unlock();
        }
    }

    @Override
    public ChunkIterator<VertexPair> unorderedFutureIterator() {
        return new StoredRecordsUnorderedIterator<VertexPair>(dbConf, storageQueryConfig, itemsCount, loadChunkSize){
            @Override
            public List<VertexPair> nextChunk() {
                List<VertexPair> chunk = null;
                try {
                    lock.lock();

                    while((chunk = super.nextChunk()).size() <= 0) notifier.await();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
                return chunk;
            }

            @Override
            public boolean hasNext() {
                try {
                    lock.lock();
                    return super.hasNext();
                } finally {
                    lock.unlock();
                }
            }
        };
    }


}
