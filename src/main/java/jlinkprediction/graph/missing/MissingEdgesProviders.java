package jlinkprediction.graph.missing;

import jlinkprediction.graph.SocialGraph;
import jlinkprediction.graph.VertexPair;

import java.util.ArrayList;
import java.util.List;

public class MissingEdgesProviders {
    public static MissingEdgesProvider Complementary() {
        return new AbstractMissingEdgesProvider() {
            @Override
            protected List<VertexPair> edgesForVertex(int currVertexIdx, int[] allVertices, SocialGraph graph) {
                final int currVertex = allVertices[currVertexIdx];
                ArrayList<VertexPair> localBuffer = new ArrayList<VertexPair>();
                for (int pairedVertexIdx = currVertexIdx + 1; pairedVertexIdx < allVertices.length; pairedVertexIdx++) {
                    VertexPair candidate = new VertexPair(currVertex, allVertices[pairedVertexIdx]);
                    if (!graph.hasEdgeBetween(candidate.firstVertex, candidate.secondVertex)) {
                        localBuffer.add(candidate);
                    }
                }
                return localBuffer;
            }
        };
    }

    public static MissingEdgesProvider Default() {
        return Complementary();
    }

    public static interface MissingEdgesProvider {
        public MissingEdgesHolder getMissingEdges(SocialGraph graph);
    }
}
