package jlinkprediction.graph.partition;

import jlinkprediction.graph.SocialGraph;

public class GraphPartitionResult {

    public final SocialGraph initialPart;
    public final SocialGraph finalPart;

    public GraphPartitionResult(SocialGraph initialPart, SocialGraph finalPart) {
        this.initialPart = initialPart;
        this.finalPart = finalPart;
    }
}
