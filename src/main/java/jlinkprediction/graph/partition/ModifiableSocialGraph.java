package jlinkprediction.graph.partition;

import jlinkprediction.graph.SocialGraph;
import jlinkprediction.graph.partition.strategy.GraphPartitionStrategies;

import java.util.HashMap;
import java.util.Map;

public abstract class ModifiableSocialGraph implements SocialGraph {

    private Map<Integer, Long> edgesCreationTime = new HashMap<Integer, Long>();

    public GraphPartitionResult divideGraph(GraphPartitionStrategies.GraphPartitionStrategy strategy) {
        return strategy.divide(this);
    }

    public abstract void addVertex(int vertex);

    public abstract int addEdgeBetween(int vertex1, int vertex2);

    protected abstract int getEdgeId(int vertex1, int vertex2);

    public int addEdgeBetween(int vertex1, int vertex2, long creationTime) {
        int edgeId = addEdgeBetween(vertex1, vertex2);
        if (creationTime >= 0) {
            edgesCreationTime.put(edgeId, creationTime);
        }
        return edgeId;
    }

    public long getEdgeCreationTime(int vertex1, int vertex2){
        Long creationTime = edgesCreationTime.get(getEdgeId(vertex1, vertex2));
        if(creationTime == null) return 0;
        return creationTime;
    }
}
