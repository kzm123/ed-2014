package jlinkprediction.graph.partition.strategy;

import jlinkprediction.graph.VertexPair;
import jlinkprediction.graph.impl.SocialGraphs;
import jlinkprediction.graph.partition.GraphPartitionResult;
import jlinkprediction.graph.partition.ModifiableSocialGraph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

class EdgesPartitionStrategy implements GraphPartitionStrategies.GraphPartitionStrategy {
    private final EdgeFilter edgeFilter;


    private EdgesPartitionStrategy(EdgeFilter edgeFilter) {
        this.edgeFilter = edgeFilter;
    }

    static EdgesPartitionStrategy Volume(int desirablePercents, float randomFactor) {
        return new EdgesPartitionStrategy(new EdgeCountFilter(desirablePercents, randomFactor));
    }

    static EdgesPartitionStrategy CreationTime(TimeMarker timeMarker) {
        return new EdgesPartitionStrategy(new EdgeCreationTimeFilter(timeMarker));
    }

    @Override
    public GraphPartitionResult divide(ModifiableSocialGraph originalGraph) {
        ModifiableSocialGraph initialGraph = SocialGraphs.Default(originalGraph.getNetworkName() + "-initialpart", originalGraph.getDescription());

        ArrayList<VertexPair> edges = new ArrayList<VertexPair>(originalGraph.getEdgesCount());

        Iterator<VertexPair> edgeIt = originalGraph.getEdges();
        while (edgeIt.hasNext()) {
            VertexPair e = edgeIt.next();
            edges.add(e);
            initialGraph.addVertex(e.firstVertex);
            initialGraph.addVertex(e.secondVertex);
        }

        for (VertexPair edge : edgeFilter.filterEdges(originalGraph, edges)) {
            initialGraph.addEdgeBetween(edge.firstVertex, edge.secondVertex);
        }

        return new GraphPartitionResult(initialGraph, originalGraph);
    }

    private static interface EdgeFilter {
        public ArrayList<VertexPair> filterEdges(ModifiableSocialGraph inputGraph, ArrayList<VertexPair> edges);
    }

    private static class EdgeCountFilter implements EdgeFilter {

        private final float edgesToStayPercent;
        private final float randomFactor;


        public EdgeCountFilter(int desirableEdgesToStayPercents, float randomFactor) {
            this.edgesToStayPercent = (float) (desirableEdgesToStayPercents / 100.0);
            this.randomFactor = randomFactor;
        }

        @Override
        public ArrayList<VertexPair> filterEdges(final ModifiableSocialGraph inputGraph, ArrayList<VertexPair> edges) {//TODO try not to delete edges from one-degree vertexes
            final int filteredEdgesCount = (int) (edgesToStayPercent * edges.size());
            ArrayList<VertexPair> edgesToStay = new ArrayList<VertexPair>();
            ArrayList<VertexPair> stayCandidates = new ArrayList<VertexPair>();

            for(VertexPair vp : edges){
                if(inputGraph.getVertexDegree(vp.firstVertex) == 1 || inputGraph.getVertexDegree(vp.secondVertex) == 1){
                    edgesToStay.add(vp);
                }else{
                    stayCandidates.add(vp);
                }
            }

            if(edgesToStay.size() == filteredEdgesCount) return edgesToStay;

            int replacementsCount = (int) (randomFactor * stayCandidates.size());
            final int stayCandidatesSize = stayCandidates.size();
            Random rand = new Random();
            while (replacementsCount-- > 0) {
                int f = rand.nextInt(stayCandidatesSize);
                int s = rand.nextInt(stayCandidatesSize);

                VertexPair tmp = stayCandidates.get(f);
                stayCandidates.set(f, stayCandidates.get(s));
                stayCandidates.set(s, tmp);
            }

            edgesToStay.addAll(stayCandidates.subList(0, filteredEdgesCount - edgesToStay.size()));

            return edgesToStay;
        }
    }

    private static class EdgeCreationTimeFilter implements EdgeFilter {

        private final TimeMarker timeMarker;

        public EdgeCreationTimeFilter(TimeMarker timeMarker) {
            this.timeMarker = timeMarker;
        }


        @Override
        public ArrayList<VertexPair> filterEdges(ModifiableSocialGraph inputGraph, ArrayList<VertexPair> edges) {
            ArrayList<VertexPair> result = new ArrayList<VertexPair>();
            long timeMillis = timeMarker.getTimestamp().getTime();
            for(VertexPair edge : edges){
                if(inputGraph.getEdgeCreationTime(edge.firstVertex, edge.secondVertex) < timeMillis){
                    result.add(edge);
                }
            }
            return result;
        }
    }

}
