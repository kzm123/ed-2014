package jlinkprediction.graph.partition.strategy;


import jlinkprediction.graph.partition.GraphPartitionResult;
import jlinkprediction.graph.partition.ModifiableSocialGraph;

public final class GraphPartitionStrategies {
    public static interface GraphPartitionStrategy {
        public GraphPartitionResult divide(ModifiableSocialGraph originalGraph);
    }


    public static GraphPartitionStrategy EdgesNumberBasedDivision(int initialPartPercentageVolume, float randomFactor) {
        return EdgesPartitionStrategy.Volume(initialPartPercentageVolume, randomFactor);
    }

    public static GraphPartitionStrategy EdgesCreationTimeBasedDivision(TimeMarker timeMarker){
        return EdgesPartitionStrategy.CreationTime(timeMarker);
    }
}
