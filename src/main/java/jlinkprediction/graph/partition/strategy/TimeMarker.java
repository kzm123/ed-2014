package jlinkprediction.graph.partition.strategy;

import java.sql.Timestamp;

public class TimeMarker {
    private int year;
    private int month;
    private int day;

    public TimeMarker(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @SuppressWarnings("deprecation")
    public Timestamp getTimestamp() {
        return new Timestamp(year - 1900, month - 1, day, 0, 0, 0, 0);
    }
}
