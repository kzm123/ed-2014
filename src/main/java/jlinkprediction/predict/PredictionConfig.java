package jlinkprediction.predict;

import jlinkprediction.graph.SocialGraph;
import jlinkprediction.graph.VertexPair;
import jlinkprediction.graph.missing.MissingEdgesHolder;
import jlinkprediction.graph.missing.MissingEdgesProviders;
import jlinkprediction.tools.ChunkIterator;

public class PredictionConfig {
    private final SocialGraph graph;
    private final MissingEdgesHolder edgesStorage;

    private PredictionConfig(SocialGraph graph, MissingEdgesProviders.MissingEdgesProvider edgesProvider) {
        this.graph = graph;
        this.edgesStorage = edgesProvider.getMissingEdges(graph);
    }

    public SocialGraph getGraph(){
        return graph;
    }

    public ChunkIterator<VertexPair> edgesIterator(){
        return edgesStorage.unorderedFutureIterator();
    }

    public static PredictionConfig NewConfig(SocialGraph graph, MissingEdgesProviders.MissingEdgesProvider edgesProvider){
        return new PredictionConfig(graph, edgesProvider);
    }
}
