package jlinkprediction.predict;

import jlinkprediction.AppConfig;
import jlinkprediction.graph.SocialGraph;
import jlinkprediction.graph.VertexPair;
import jlinkprediction.predict.store.PredictedEdge;
import jlinkprediction.predict.store.ResultHolder;
import jlinkprediction.similarity.VertexSimilarityIndex;
import jlinkprediction.tools.ChunkIterator;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class Predictor {

    private VertexSimilarityIndex simIndex;


    public Predictor withSimilarityIdx(VertexSimilarityIndex index) {
        this.simIndex = index;
        return this;
    }

    public ResultHolder predictLinks(PredictionConfig pConfig) {
        if (simIndex == null) throw new RuntimeException("Similarity index not set");

        final ChunkIterator<VertexPair> edgeIter = pConfig.edgesIterator();
        final int coreNumbers = Runtime.getRuntime().availableProcessors();
        final ReentrantLock outputQueueLock = new ReentrantLock();

        ResultHolder resultSet = new ResultHolder(AppConfig.DB_CONFIG);

        ConcurrentRatingWorker workers[] = new ConcurrentRatingWorker[coreNumbers];

        for (int i = 0; i < coreNumbers; ++i) {
            workers[i] = new ConcurrentRatingWorker(i, outputQueueLock, edgeIter, resultSet, pConfig.getGraph(), AppConfig.PREDICTOR_EDGES_CHUNK_SIZE, simIndex);
        }

        for (int i = 0; i < coreNumbers; ++i) {
            workers[i].start();
        }

        for (int i = 0; i < coreNumbers; ++i) {
            try {
                workers[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return resultSet;
    }

    private static class ConcurrentRatingWorker extends Thread {

        private static final int LOCAL_THREAD_BUFF_FLUSH_THRESHOLD = 50000;

        private final VertexSimilarityIndex simIndex;
        private ReentrantLock outputQueueLock;
        private ChunkIterator<VertexPair> inputQueue;
        private ResultHolder outputQueue;
        private SocialGraph graph;
        private int chunkSize;

        public ConcurrentRatingWorker(
                                      int idx,
                                      ReentrantLock outputQueueLock,
                                      ChunkIterator<VertexPair> inputQueue,
                                      ResultHolder outputQueue,
                                      SocialGraph graph,
                                      int chunkSize,
                                      VertexSimilarityIndex similarityIndex) {
            super("RatingWorker-" + idx + " " +similarityIndex.indexName());
            this.simIndex = similarityIndex;
            this.outputQueueLock = outputQueueLock;
            this.inputQueue = inputQueue;
            this.outputQueue = outputQueue;
            this.graph = graph;
            this.chunkSize = chunkSize;
        }

        @Override
        public void run() {
            ArrayList<PredictedEdge> predictedEdges = new ArrayList<PredictedEdge>();
            while (true) {
                ArrayList<VertexPair> vpairs = getNextVertexesPairs();
                if (vpairs.size() == 0) break;


                for (VertexPair vp : vpairs) {
                    PredictedEdge pe = new PredictedEdge(vp.firstVertex, vp.secondVertex, simIndex.compute(vp.firstVertex, vp.secondVertex, graph));
                    if (pe.rankIndex > 0) {
                        predictedEdges.add(pe);
                    }
                }

                if(predictedEdges.size() >= LOCAL_THREAD_BUFF_FLUSH_THRESHOLD){
                    putIntoResultSet(predictedEdges);
                    predictedEdges.clear();
                }
            }

            putIntoResultSet(predictedEdges);
        }

        private ArrayList<VertexPair> getNextVertexesPairs() {
            ArrayList<VertexPair> result = new ArrayList<VertexPair>();

            while (result.size() < chunkSize && inputQueue.hasNext()) {
                result.addAll(inputQueue.nextChunk());
            }

            return result;
        }

        private void putIntoResultSet(ArrayList<PredictedEdge> predictedEdges) {
            try {
                outputQueueLock.lock();
                outputQueue.storeElements(predictedEdges);
            } finally {
                outputQueueLock.unlock();
            }
        }


    }
}
