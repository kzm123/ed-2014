package jlinkprediction.predict.filter;

import jlinkprediction.predict.store.PredictedEdge;
import jlinkprediction.tools.ChunkIterator;

import java.util.ArrayList;
import java.util.List;

class BestResultsCountFilter implements ResultFilters.ResultFilter {
    private int maxCount;

    public BestResultsCountFilter(int maxEdgesCount) {
        this.maxCount = maxEdgesCount;
    }

    @Override
    public ArrayList<PredictedEdge> filterResults(ChunkIterator<PredictedEdge> resultIterator) {
        ArrayList<PredictedEdge> resultList = new ArrayList<PredictedEdge>();

        while(resultList.size() < maxCount && resultIterator.hasNext()){
            List<PredictedEdge> chunk = resultIterator.nextChunk();
            if(chunk.size() > maxCount){
                resultList.addAll(chunk.subList(0, maxCount - resultList.size()));
            }else{
                resultList.addAll(chunk);
            }
        }

        return resultList;
    }

    @Override
    public String label() {
        return "'BestRanked " + maxCount +"'";
    }

}
