package jlinkprediction.predict.filter;


import jlinkprediction.predict.store.PredictedEdge;
import jlinkprediction.tools.ChunkIterator;

import java.util.ArrayList;
import java.util.List;

class RankRelativeThresholdFilter implements ResultFilters.ResultFilter {
    private final double percentageOfMaxRated;

    public RankRelativeThresholdFilter(double percentageOfMaxRated) {
        this.percentageOfMaxRated = percentageOfMaxRated;
    }

    @Override
    public ArrayList<PredictedEdge> filterResults(ChunkIterator<PredictedEdge> resultIterator) {
        ArrayList<PredictedEdge> result = new ArrayList<PredictedEdge>();
        if (resultIterator.hasNext()) {
            List<PredictedEdge> currentChunk = resultIterator.nextChunk();
            final double threshold = currentChunk.get(0).rankIndex * percentageOfMaxRated;

            adding:
            while (true) {
                for (PredictedEdge pe : currentChunk) {
                    if (pe.rankIndex >= threshold) {
                        result.add(pe);
                    } else {
                        break adding;
                    }
                }
                if (resultIterator.hasNext()) {
                    currentChunk = resultIterator.nextChunk();
                } else {
                    break adding;
                }
            }
        }
        return result;
    }

    @Override
    public String label() {
        return "'RankThreshold " + percentageOfMaxRated * 100 + "% of max'";
    }
}
