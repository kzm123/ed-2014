package jlinkprediction.predict.filter;

import jlinkprediction.predict.store.PredictedEdge;
import jlinkprediction.tools.ChunkIterator;

import java.util.ArrayList;

public class ResultFilters {

    public static interface ResultFilter {
        public ArrayList<PredictedEdge> filterResults(ChunkIterator<PredictedEdge> resultIterator);
        public String label();
    }

    public static ResultFilter BestRankRelative(float percentageOfMaxRated){
        return new RankRelativeThresholdFilter(percentageOfMaxRated);
    }

    public static ResultFilter BestRanked(int numberOfBestRanked){
        return new BestResultsCountFilter(numberOfBestRanked);
    }
}
