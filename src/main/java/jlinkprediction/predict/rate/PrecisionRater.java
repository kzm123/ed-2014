package jlinkprediction.predict.rate;

import jlinkprediction.graph.SocialGraph;
import jlinkprediction.predict.filter.ResultFilters;
import jlinkprediction.predict.store.PredictedEdge;
import jlinkprediction.predict.store.ResultHolder;

import java.util.ArrayList;

public class PrecisionRater implements Rater {

    @Override
    public double rate(SocialGraph originalGraph, ResultHolder resultHolder, ResultFilters.ResultFilter resultFilter) {
        ArrayList<PredictedEdge> predictionsList = resultFilter.filterResults(resultHolder.orderedIterator());
        if (predictionsList.size() == 0) return 0;

        int rightPredictionCounter = 0;
        for (PredictedEdge pe : predictionsList) {
            if (originalGraph.hasEdgeBetween(pe.vertex1, pe.vertex2)) {
                rightPredictionCounter++;
            }
        }
        return (double) rightPredictionCounter * 100 / predictionsList.size();
    }
}
