package jlinkprediction.predict.rate;

import jlinkprediction.graph.SocialGraph;
import jlinkprediction.predict.filter.ResultFilters;
import jlinkprediction.predict.store.ResultHolder;

public interface Rater {
    public double rate(SocialGraph originalGraph, ResultHolder resultHolder, ResultFilters.ResultFilter resultFilter);
}
