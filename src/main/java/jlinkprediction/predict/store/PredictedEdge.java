package jlinkprediction.predict.store;


public class PredictedEdge {
    public final int vertex1;
    public final int vertex2;
    public final double rankIndex;

    public PredictedEdge(int vertex1, int vertex2, double rankIndex) {
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
        this.rankIndex = rankIndex;
    }

    @Override
    public String toString() {
        return vertex1 + " : " + vertex2 + " -> " + rankIndex;
    }
}
