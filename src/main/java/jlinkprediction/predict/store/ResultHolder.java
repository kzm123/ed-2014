package jlinkprediction.predict.store;

import jlinkprediction.AppConfig;
import jlinkprediction.db.DbConfig;
import jlinkprediction.tools.storage.DbRecordBufferedStorage;
import jlinkprediction.tools.storage.StorageQueryConfig;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultHolder extends DbRecordBufferedStorage<PredictedEdge> {

    private static class PredictedEdgeQueryConfig extends StorageQueryConfig<PredictedEdge>{
        private static final String RECORD_ATTRIBUTES = "v1,v2,rank";
        private static final String LOAD_ORDERING_QUERY_PART = "rank desc";


        public PredictedEdgeQueryConfig() {
            super("predicted_edges", "predicted_edge_idx");
        }

        @Override
        protected String dbRecordValueToStore(PredictedEdge record) {
            return new StringBuilder()
                    .append(record.vertex1).append(",").append(record.vertex2).append(",").append(record.rankIndex)
                    .toString();
        }

        @Override
        protected String dbRecordValueAttributeNamesToStore() {
            return RECORD_ATTRIBUTES;
        }

        @Override
        protected PredictedEdge createFromQueryResult(ResultSet resultSet) throws SQLException {
            return new PredictedEdge(resultSet.getInt(1), resultSet.getInt(2), resultSet.getDouble(3));
        }

        @Override
        protected String loadOrderedQueryPart() {
            return LOAD_ORDERING_QUERY_PART;
        }
    }

    public ResultHolder(DbConfig conf) {
        super(conf, new PredictedEdgeQueryConfig(), AppConfig.PREDICTED_EDGES_BUFF_FLUSH_THRESHOLD, AppConfig.PREDICTED_EDGES_ITERATOR_CHUNK_SIZE);
    }
}
