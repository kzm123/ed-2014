package jlinkprediction.similarity;

import jlinkprediction.graph.SocialGraph;

public enum LocalSimilarityIndex implements VertexSimilarityIndex {
    CN(
            "Common Neighbours",
            new Worker() {
                public double compute(int vertex1, int vertex2, SocialGraph graph) {
                    return graph.commonNeighboursCount(vertex1, vertex2);
                }
            }
    ),
    SALTON(
            "Salton Index",
            new Worker() {
                public double compute(int vertex1, int vertex2, SocialGraph graph) {
                    final int cnc = graph.commonNeighboursCount(vertex1, vertex2);
                    final double sqrt = Math.sqrt(graph.getVertexDegree(vertex1) * graph.getVertexDegree(vertex2));
                    if(cnc * sqrt == 0) return 0;
                    return cnc / sqrt;
                }
            }
    ),
    JACCARD(
            "Jaccard Index",
            new Worker() {
                public double compute(int vertex1, int vertex2, SocialGraph graph) {
                    final int cnc = graph.commonNeighboursCount(vertex1, vertex2);
                    final int anc = graph.allNeighboursCount(vertex1, vertex2);
                    if(cnc * anc == 0) return 0;
                    return cnc / anc;
                }
            }
    ),
    SORENSEN(
            "Sorensen Index",
            new Worker() {
                public double compute(int vertex1, int vertex2, SocialGraph graph) {
                    final int cnc = graph.commonNeighboursCount(vertex1, vertex2);
                    final int dsum = graph.getVertexDegree(vertex1) + graph.getVertexDegree(vertex2);
                    if(cnc * dsum == 0) return 0;
                    return 2 * cnc / dsum;
                }
            }
    ),
    HPI(
            "Hub Promoted Index",
            new Worker() {
                public double compute(int vertex1, int vertex2, SocialGraph graph) {
                    final int cnc = graph.commonNeighboursCount(vertex1, vertex2);
                    final int minD = Math.min(graph.getVertexDegree(vertex1), graph.getVertexDegree(vertex2));
                    if(cnc * minD == 0) return 0;
                    return cnc / minD;
                }
            }
    ),
    HDI(
            "Hub Depresed Index",
            new Worker() {
                public double compute(int vertex1, int vertex2, SocialGraph graph) {
                    final int cnc = graph.commonNeighboursCount(vertex1, vertex2);
                    final int maxD = Math.max(graph.getVertexDegree(vertex1), graph.getVertexDegree(vertex2));
                    if(cnc * maxD == 0) return 0;
                    return cnc / maxD;
                }
            }
    ),
    LHNI(
            "Leicht-Holme-Newman Index",
            new Worker() {
                public double compute(int vertex1, int vertex2, SocialGraph graph) {
                    final int cnc = graph.commonNeighboursCount(vertex1, vertex2);
                    final int dm = graph.getVertexDegree(vertex1) * graph.getVertexDegree(vertex2);
                    if(cnc * dm == 0) return 0;
                    return cnc / dm;
                }
            }
    ),
    PA(
            "Preferential Attachment Index",
            new Worker() {
                public double compute(int vertex1, int vertex2, SocialGraph graph) {
                    return graph.getVertexDegree(vertex1) * graph.getVertexDegree(vertex2);
                }
            }
    ),
    AA(
            "Adamic-Adar Index",
            new Worker() {
                public double compute(int vertex1, int vertex2, SocialGraph graph) {
                    double result = 0;
                    for (int vertex : graph.commonNeighbours(vertex1, vertex2).toIntArray()) {
                        final int vertexDegree = graph.getVertexDegree(vertex);
                        result += (vertexDegree == 1) ? 0 : 1.0 / Math.log(vertexDegree);
                    }
                    return result;
                }
            }
    ),
    RA(
            "Resource Allocation Index",
            new Worker() {
                public double compute(int vertex1, int vertex2, SocialGraph graph) {
                    double result = 0;
                    for (int vertex : graph.commonNeighbours(vertex1, vertex2).toIntArray()) {
                        final int vertexDegree = graph.getVertexDegree(vertex);
                        if(vertexDegree != 0) result += 1.0 / vertexDegree;
                    }
                    return result;
                }
            }
    ),;
    private String indexName;
    private Worker computeEngine;
    private LocalSimilarityIndex(String name, Worker computeEngine) {
        this.indexName = name;
        this.computeEngine = computeEngine;
    }

    public double compute(int vertex1, int vertex2, SocialGraph graph) {
        return computeEngine.compute(vertex1, vertex2, graph);
    }

    @Override
    public String indexName() {
        return indexName;
    }

    public SimilarityIndexType getType() {
        return SimilarityIndexType.LOCAL;
    }

    private interface Worker {
        public double compute(int vertex1, int vertex2, SocialGraph graph);
    }

}
