package jlinkprediction.similarity;

public enum SimilarityIndexType {
    LOCAL("Local"),
    GLOBAL("Global"),
    QUASI_LOCAL("Quasi-Local"),;

    private String typeName;

    private SimilarityIndexType(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return this.typeName;
    }
}
