package jlinkprediction.similarity;

import jlinkprediction.graph.SocialGraph;

public interface VertexSimilarityIndex {
    public double compute(int vertex1, int vertex2, SocialGraph graph);

    public SimilarityIndexType getType();

    public String indexName();
}
