package jlinkprediction.tools;


import java.util.List;

public interface ChunkIterator<T> {
    public List<T> nextChunk();
    public boolean hasNext();
}
