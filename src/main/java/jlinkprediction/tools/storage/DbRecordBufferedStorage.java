package jlinkprediction.tools.storage;

import jlinkprediction.db.DbConfig;
import jlinkprediction.db.PostgresqlAbstractConnector;
import jlinkprediction.tools.ChunkIterator;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public abstract class DbRecordBufferedStorage<T> extends PostgresqlAbstractConnector{

    protected final int flushBuffSizeThreshold;
    protected final int loadChunkSize;
    protected final StorageQueryConfig<T> storageQueryConfig;
    protected long itemsCount;
    private final boolean isFuture;

    private ArrayList<T> storeBuffer = new ArrayList<T>();

    public DbRecordBufferedStorage(DbConfig conf, StorageQueryConfig<T> queryConfig, int flushBufferSize, int loadChunkSize){
        this(conf, queryConfig, flushBufferSize, loadChunkSize, -1);
    }

    public DbRecordBufferedStorage(DbConfig conf, StorageQueryConfig<T> queryConfig, int flushBufferSize, int loadChunkSize, long expectedElementsCount) {
        super(conf);

        this.storageQueryConfig = queryConfig;
        this.flushBuffSizeThreshold = flushBufferSize;
        this.loadChunkSize = loadChunkSize;
        this.itemsCount = Math.max(expectedElementsCount, 0);
        this.isFuture = itemsCount != 0;

        statementInvocation(new StatementConsumer() {
            @Override
            public void consumeStatement(Statement statement) throws SQLException {
                statement.execute(storageQueryConfig.resetStorageQuery());
            }
        });
    }

    public void flushResults(boolean force) {
        if (force && storeBuffer.size() > 0 || storeBuffer.size() >= flushBuffSizeThreshold) {
            statementInvocation(new StatementConsumer() {
                @Override
                public void consumeStatement(Statement statement) throws SQLException {
                   statement.execute(storageQueryConfig.storeQuery(storeBuffer));
                }
            });
            storeBuffer = new ArrayList<T>();
        }
    }

    public void storeElements(Collection<T> elementsToStore){
        storeElements(elementsToStore, false);
    }

    public void storeElements(Collection<T> elementsToStore, boolean forceFlush){
        if(!isFuture) itemsCount += elementsToStore.size();
        storeBuffer.addAll(elementsToStore);
        flushResults(forceFlush);
    }

    public ChunkIterator<T> orderedIterator(){
        flushResults(true);
        return new StoredRecordsOrderedIterator(dbConf, storageQueryConfig, itemsCount, loadChunkSize);
    }

    public ChunkIterator<T> unorderedFutureIterator(){
        return new StoredRecordsUnorderedIterator(dbConf, storageQueryConfig, itemsCount, loadChunkSize);
    }

}
