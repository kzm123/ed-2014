package jlinkprediction.tools.storage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

public abstract class StorageQueryConfig<T> {
    private final String tableName;
    private final String indexingSequenceName;

    public StorageQueryConfig(String tableName, String indexingSequenceName) {
        this.indexingSequenceName = indexingSequenceName;
        this.tableName = tableName;
    }

    public String resetStorageQuery(){
        return new StringBuilder()
                    .append("delete from ").append(tableName).append(";")
                    .append("alter sequence ").append(indexingSequenceName).append(" restart;")
                .toString();
    }

    protected abstract String dbRecordValueToStore(T record);
    protected abstract String dbRecordValueAttributeNamesToStore();
    protected abstract T createFromQueryResult(ResultSet resultSet) throws SQLException;
    protected abstract String loadOrderedQueryPart();


    public String storeQuery(Collection<T> elements){
        StringBuilder query = new StringBuilder();

        query
                .append("insert into ").append(tableName).append("(")
                .append(dbRecordValueAttributeNamesToStore())
                .append(") values ");
        for(T element : elements){
            query.append("(").append(dbRecordValueToStore(element)).append("),");
        }
        query.replace(query.length() - 1, query.length(), ";");
        //System.out.println(query); TODO
        return query.toString();
    }

    public String getLoadOrderedQuery(long currentSize){
        return new StringBuilder()
                .append("select ").append(dbRecordValueAttributeNamesToStore()).append(" from ").append(tableName)
                .append(" where idx < ").append(currentSize)
                .append(" order by ").append(loadOrderedQueryPart()).append(", idx asc ")
                .toString();
    }

    public String getLoadChunkedQuery(long startIdx, long maxElements){
        return new StringBuilder()
                .append("select ").append(dbRecordValueAttributeNamesToStore()).append(" from ").append(tableName)
                .append(" where idx >= ").append(startIdx).append(" and idx < ").append(startIdx + maxElements).append(";")
                .toString();
    }
}
