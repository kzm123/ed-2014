package jlinkprediction.tools.storage;

import jlinkprediction.db.DbConfig;
import jlinkprediction.db.PostgresqlAbstractConnector;
import jlinkprediction.tools.ChunkIterator;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;


class StoredRecordsOrderedIterator<T> extends PostgresqlAbstractConnector implements ChunkIterator<T> {

    private final long storedElements;
    private long currentChunkNumber;
    private final long loadChunkSize;
    private final StorageQueryConfig storageQueryConfig;
    private final Deque<List<T>> chunksQueue;
    private final int chunksToLoad;

    public StoredRecordsOrderedIterator(DbConfig config, StorageQueryConfig storageQueryConfig, long storedElements, long loadChunkSize) {
        super(config);
        this.storageQueryConfig = storageQueryConfig;
        this.loadChunkSize = loadChunkSize;
        this.currentChunkNumber = 0;
        this.storedElements = storedElements;
        this.chunksQueue = new LinkedList<List<T>>();
        this.chunksToLoad = Runtime.getRuntime().availableProcessors();
    }

    @Override
    public List<T> nextChunk() {
        if(chunksQueue.size() == 0) {
            StoredRecordsOrderedLoader loader = new StoredRecordsOrderedLoader(storageQueryConfig, storedElements, loadChunkSize, currentChunkNumber++);
            statementInvocation(loader);
            ArrayList<T> bigChunk = loader.getLoadedRecords();

            if(bigChunk.size() == 0) return new ArrayList<T>();

            final int smallChunkSize = Math.max(bigChunk.size() / chunksToLoad, 1);

            int idx = 0;
            for(; idx + smallChunkSize < bigChunk.size(); idx += smallChunkSize) {
                chunksQueue.addLast(bigChunk.subList(idx, idx + smallChunkSize));
            }

            if(idx < bigChunk.size()){
                chunksQueue.addLast(bigChunk.subList(idx, bigChunk.size()));
            }

            chunksQueue.addLast(bigChunk);
        }
        return chunksQueue.poll();
    }

    @Override
    public boolean hasNext() {
        return currentChunkNumber * (long) loadChunkSize < storedElements;
    }
}
