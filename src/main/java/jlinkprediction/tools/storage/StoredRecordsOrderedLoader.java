package jlinkprediction.tools.storage;

import jlinkprediction.db.PostgresqlAbstractConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

class StoredRecordsOrderedLoader<T> implements PostgresqlAbstractConnector.StatementConsumer {
    private final ArrayList<T> loadedRecords = new ArrayList<T>();
    private final long chunkSize;
    private final long chunkNumber;
    private final long currentSize;
    private final StorageQueryConfig<T> storageQueryConfig;

    public StoredRecordsOrderedLoader(StorageQueryConfig<T> storageQueryConfig, long currentSize, long chunkSize, long chunkNumber) {
        this.storageQueryConfig = storageQueryConfig;
        this.chunkSize = chunkSize;
        this.chunkNumber = chunkNumber;
        this.currentSize = currentSize;
    }

    @Override
    public void consumeStatement(Statement statement) throws SQLException {
        StringBuilder query = new StringBuilder()
                .append(storageQueryConfig.getLoadOrderedQuery(currentSize))
                .append(" limit ").append(chunkSize)
                .append(" offset ").append(chunkNumber * chunkSize)
                ;
        ResultSet resultSet = statement.executeQuery(query.toString());
        while (resultSet.next()) {
            loadedRecords.add(storageQueryConfig.createFromQueryResult(resultSet));
        }
    }

    public ArrayList<T> getLoadedRecords() {
        return this.loadedRecords;
    }

}
