package jlinkprediction.tools.storage;

import jlinkprediction.db.DbConfig;
import jlinkprediction.db.PostgresqlAbstractConnector;
import jlinkprediction.tools.ChunkIterator;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class StoredRecordsUnorderedIterator<T> extends PostgresqlAbstractConnector implements ChunkIterator<T> {

    private final long expectedRecordsNumber;
    private long currentIdx;
    private final int loadChunkSize;
    private final StorageQueryConfig<T> storageQueryConfig;
    private final Deque<List<T>> chunksQueue;
    private final int chunksToLoad;

    public StoredRecordsUnorderedIterator(DbConfig config, StorageQueryConfig<T> storageQueryConfig, long expectedRecordsNumber, int loadChunkSize) {
        super(config);
        this.storageQueryConfig = storageQueryConfig;
        this.expectedRecordsNumber = expectedRecordsNumber;
        this.loadChunkSize = loadChunkSize;
        this.currentIdx = 0;
        this.chunksQueue = new LinkedList<List<T>>();
        this.chunksToLoad = Runtime.getRuntime().availableProcessors();
    }

    @Override
    public List<T> nextChunk() {
        if(chunksQueue.size() == 0) {
            StoredRecordsUnorderedLoader loader = new StoredRecordsUnorderedLoader(storageQueryConfig, currentIdx, loadChunkSize );
            statementInvocation(loader);
            ArrayList<T> bigChunk = loader.getLoadedRecords();
            currentIdx += bigChunk.size();
            if(bigChunk.size() == 0) return new ArrayList<T>();

            final int smallChunkSize = Math.max(bigChunk.size() / chunksToLoad, 1);

            int idx = 0;
            for(; idx + smallChunkSize < bigChunk.size(); idx += smallChunkSize) {
                chunksQueue.addLast(bigChunk.subList(idx, idx + smallChunkSize));
            }

            if(idx < bigChunk.size()){
                chunksQueue.addLast(bigChunk.subList(idx, bigChunk.size()));
            }

            chunksQueue.addLast(bigChunk);
        }
        return chunksQueue.poll();
    }

    @Override
    public boolean hasNext() {
        return currentIdx < expectedRecordsNumber;
    }
}
