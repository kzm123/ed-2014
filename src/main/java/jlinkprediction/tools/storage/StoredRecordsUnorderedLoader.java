package jlinkprediction.tools.storage;

import jlinkprediction.db.PostgresqlAbstractConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class StoredRecordsUnorderedLoader<T> implements PostgresqlAbstractConnector.StatementConsumer {
    private final ArrayList<T> loadedRecords = new ArrayList<T>();
    private final long currentIdx;
    private final int requestedChunkSize;
    private final StorageQueryConfig<T> storageQueryConfig;

    public StoredRecordsUnorderedLoader(StorageQueryConfig<T> storageQueryConfig, long currentIdx, int requestedChunkSize) {
        this.storageQueryConfig = storageQueryConfig;
        this.currentIdx = currentIdx;
        this.requestedChunkSize = requestedChunkSize;
    }

    @Override
    public void consumeStatement(Statement statement) throws SQLException {
        StringBuilder query = new StringBuilder()
                .append(storageQueryConfig.getLoadChunkedQuery(currentIdx, requestedChunkSize));

        ResultSet resultSet = statement.executeQuery(query.toString());
        while (resultSet.next()) {
            loadedRecords.add(storageQueryConfig.createFromQueryResult(resultSet));
        }
    }

    public ArrayList<T> getLoadedRecords() {
        return this.loadedRecords;
    }
}
