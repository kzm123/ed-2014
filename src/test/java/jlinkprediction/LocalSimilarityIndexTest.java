package jlinkprediction;

import jlinkprediction.graph.SocialGraph;
import jlinkprediction.similarity.LocalSimilarityIndex;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import toools.set.IntHashSet;
import toools.set.IntSet;

import java.util.HashMap;
import java.util.Map.Entry;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class LocalSimilarityIndexTest {

    static private final int v1 = 1;
    static private final int v2 = 2;
    static private final int v1Degree = 4;
    static private final int v2Degree = 6;
    static private final int sumneighbours = 7;
    static HashMap<Integer, Integer> commonNeighboursDegrees = new HashMap<Integer, Integer>();
    static {
        commonNeighboursDegrees.put(3, 5);
        commonNeighboursDegrees.put(4, 7);
        commonNeighboursDegrees.put(5, 41);


        commonneighbours = commonNeighboursDegrees.size();
    }
    static private int commonneighbours;
    @Mock
    SocialGraph graphMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);


        IntSet commonNeigbours = new IntHashSet();
        for (Entry<Integer, Integer> e : commonNeighboursDegrees.entrySet()) {
            commonNeigbours.add(e.getKey());
            when(graphMock.getVertexDegree(e.getKey())).thenReturn(e.getValue());
        }

        when(graphMock.commonNeighbours(v1, v2)).thenReturn(commonNeigbours);
        when(graphMock.commonNeighbours(v2, v1)).thenReturn(commonNeigbours);

        when(graphMock.commonNeighboursCount(v1, v2)).thenReturn(commonneighbours);
        when(graphMock.commonNeighboursCount(v2, v1)).thenReturn(commonneighbours);

        when(graphMock.allNeighboursCount(v1, v2)).thenReturn(sumneighbours);
        when(graphMock.allNeighboursCount(v2, v1)).thenReturn(sumneighbours);

        when(graphMock.getVertexDegree(v1)).thenReturn(v1Degree);
        when(graphMock.getVertexDegree(v2)).thenReturn(v2Degree);


    }

    @Test
    public void testCN() {
        LocalSimilarityIndex tested = LocalSimilarityIndex.CN;
        assertEquals(tested.compute(v1, v2, graphMock), tested.compute(v2, v1, graphMock));

        assertEquals(tested.compute(v1, v2, graphMock), commonneighbours);
    }

    @Test
    public void testSALTON() {
        LocalSimilarityIndex tested = LocalSimilarityIndex.SALTON;
        assertEquals(tested.compute(v1, v2, graphMock), tested.compute(v2, v1, graphMock));

        assertEquals(tested.compute(v1, v2, graphMock), ((double) commonneighbours) / Math.sqrt(v1Degree * v2Degree));
    }

    @Test
    public void testJACCARD() {
        LocalSimilarityIndex tested = LocalSimilarityIndex.JACCARD;
        assertEquals(tested.compute(v1, v2, graphMock), tested.compute(v2, v1, graphMock));

        assertEquals(tested.compute(v1, v2, graphMock), ((double) commonneighbours) / sumneighbours);
    }

    @Test
    public void testSORENSEN() {
        LocalSimilarityIndex tested = LocalSimilarityIndex.SORENSEN;
        assertEquals(tested.compute(v1, v2, graphMock), tested.compute(v2, v1, graphMock));

        assertEquals(tested.compute(v1, v2, graphMock), 2.0 * commonneighbours / (v1Degree + v2Degree));
    }

    @Test
    public void testHPI() {
        LocalSimilarityIndex tested = LocalSimilarityIndex.HPI;
        assertEquals(tested.compute(v1, v2, graphMock), tested.compute(v2, v1, graphMock));

        assertEquals(tested.compute(v1, v2, graphMock), ((double) commonneighbours) / Math.min(v1Degree, v2Degree));
    }

    @Test
    public void testHDI() {
        LocalSimilarityIndex tested = LocalSimilarityIndex.HDI;
        assertEquals(tested.compute(v1, v2, graphMock), tested.compute(v2, v1, graphMock));

        assertEquals(tested.compute(v1, v2, graphMock), ((double) commonneighbours) / Math.max(v1Degree, v2Degree));
    }

    @Test
    public void testLHNI() {
        LocalSimilarityIndex tested = LocalSimilarityIndex.LHNI;
        assertEquals(tested.compute(v1, v2, graphMock), tested.compute(v2, v1, graphMock));

        assertEquals(tested.compute(v1, v2, graphMock), ((double) commonneighbours) / (v1Degree * v2Degree));
    }

    @Test
    public void testPA() {
        LocalSimilarityIndex tested = LocalSimilarityIndex.PA;
        assertEquals(tested.compute(v1, v2, graphMock), tested.compute(v2, v1, graphMock));

        assertEquals(tested.compute(v1, v2, graphMock), v1Degree * v2Degree);
    }


    @Test
    public void testAA() {
        LocalSimilarityIndex tested = LocalSimilarityIndex.AA;
        assertEquals(tested.compute(v1, v2, graphMock), tested.compute(v2, v1, graphMock));

        double computedValue = 0;
        for (int vertexDegree : commonNeighboursDegrees.values()) {
            computedValue += 1.0 / Math.log(vertexDegree);
        }
        assertEquals(tested.compute(v1, v2, graphMock), computedValue);
    }

    @Test
    public void testRA() {
        LocalSimilarityIndex tested = LocalSimilarityIndex.RA;
        assertEquals(tested.compute(v1, v2, graphMock), tested.compute(v2, v1, graphMock));

        double computedValue = 0;
        for (int vertexDegree : commonNeighboursDegrees.values()) {
            computedValue += 1.0 / vertexDegree;
        }
        assertEquals(tested.compute(v1, v2, graphMock), computedValue);
    }
}
