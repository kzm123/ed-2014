package jlinkprediction.graph.impl;

import jlinkprediction.graph.VertexPair;
import org.junit.Before;
import org.junit.Test;
import org.python.google.common.collect.Lists;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class InMemoryGRPHSocialGraphTest {
    private InMemoryGRPHSocialGraph graphMock;
    private String networkName = "test";
    private String networkDescritpion = "test";

    @Before
    public void setUp() throws Exception {
        graphMock = new InMemoryGRPHSocialGraph(networkName, networkDescritpion);
        graphMock.addVertex(1);
        graphMock.addVertex(2);
        graphMock.addVertex(3);
        graphMock.addEdgeBetween(1, 2);
        graphMock.addEdgeBetween(1, 3);
    }

    @Test
    public void testHasEdgeBetween() throws Exception {
        assertTrue(graphMock.hasEdgeBetween(1, 2));
        assertFalse(graphMock.hasEdgeBetween(2, 3));
    }

    @Test
    public void testGetEdgesToPredict() throws Exception {
        fail("NOT IMPLEMENTED");
/*        List<VertexPair> edgesList = Lists.newArrayList(graphMock.getEdgesToPredict());
        assertEquals(edgesList.size(), 1);
        VertexPair vp = edgesList.get(0);
        assertTrue(vp.firstVertex == 2 || vp.secondVertex == 2);
        assertTrue(vp.firstVertex == 3 || vp.secondVertex == 3);*/
    }

    @Test
    public void testGetVertices() throws Exception {
        Set<Integer> graphVerticesSet = new HashSet<Integer>(graphMock.getVertices().toIntegerArrayList());
        Set<Integer> verticesSet = new HashSet<Integer>(Arrays.asList(1,2,3));
        assertEquals(verticesSet, graphVerticesSet);
    }

    @Test
    public void testGetEdges() throws Exception {
        List<VertexPair> edgesList = Lists.newArrayList(graphMock.getEdges());
        assertEquals(2, edgesList.size());
    }

    @Test
    public void testAddEdgeBetween() throws Exception {
        assertEquals(2, Lists.newArrayList(graphMock.getEdges()).size());
        assertFalse(graphMock.hasEdgeBetween(2, 3));

        graphMock.addEdgeBetween(2, 3);

        assertEquals(3, Lists.newArrayList(graphMock.getEdges()).size());
        assertTrue(graphMock.hasEdgeBetween(2, 3));
    }

    @Test
    public void testGetEdgeId() throws Exception {
        int edgeId = graphMock.addEdgeBetween(2, 3);
        assertEquals(edgeId, graphMock.getEdgeId(2, 3));
    }

    @Test
    public void testCommonNeighbours() throws Exception {
        int[] neighbours = graphMock.commonNeighbours(2, 3).toIntArray();
        assertEquals(1, neighbours.length);
        assertEquals(1, neighbours[0]);
    }

    @Test
    public void testAllNeighbours() throws Exception {
        int[] neighbours = graphMock.allNeighbours(2, 3).toIntArray();
        assertEquals(1, neighbours.length);
        assertEquals(1, neighbours[0]);
    }

    @Test
    public void testGetVertexDegree() throws Exception {
        assertEquals(2, graphMock.getVertexDegree(1));
        assertEquals(1, graphMock.getVertexDegree(2));
        assertEquals(1, graphMock.getVertexDegree(3));
    }

    @Test
    public void testCommonNeighboursCount() throws Exception {
        assertEquals(1, graphMock.commonNeighboursCount(2, 3));
    }

    @Test
    public void testAllNeighboursCount() throws Exception {
        assertEquals(1, graphMock.allNeighboursCount(2, 3));
    }

    @Test
    public void testGetNetworkName() throws Exception {
        assertEquals(networkName, graphMock.getNetworkName());
    }

    @Test
    public void testGetDescription() throws Exception {
        assertEquals(networkDescritpion, graphMock.getNetworkName());
    }

    @Test
    public void testGetNumberOfEdges() throws Exception {
        assertEquals(2, graphMock.getEdgesCount());
    }

    @Test
    public void testAddVertex() throws Exception {
        assertEquals(3, graphMock.getVertices().size());
        graphMock.addVertex(4);
        assertEquals(4, graphMock.getVertices().size());
    }
}